import { Container } from "@mui/material";
import ContentComponent from "./components/content/ContentComponent";
import FooterComponent from "./components/footer/FooterComponent";
import HeaderBarComponent from "./components/header/HeaderBarComponent";

function App() {
  return (
    <Container maxWidth={false} disableGutters>
      <HeaderBarComponent/>
      <ContentComponent/>
      <FooterComponent/>
    </Container>
  );
}

export default App;
