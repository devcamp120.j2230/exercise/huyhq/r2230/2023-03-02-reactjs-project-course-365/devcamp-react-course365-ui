import { Container, Grid, Typography } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import CourseCard from "../card/CourseCard";

const RecommendedCourses = () => {
    const [courses, setCourses] = useState([])

    const axiosCall = async (url, body) => {
        const response = await axios(url, body);
        return response.data;
    };

    const getAllHandle = () => {
        axiosCall("https://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses")
            .then(result => saveDataToCourse(result))
            .catch(error => console.log('error', error.message));
    };

    const saveDataToCourse = (data) => {
        setCourses(data);
    };

    useEffect(() => {
        getAllHandle()
      },[]);

    return (
        <Container maxWidth="lg" >
            <Typography variant="h5" mt={5} pl={3}>Recomment to you</Typography>
            <Grid container>
                {courses.slice(0, 4).map((val, icon) => {
                    return <CourseCard key={icon}
                        courseName={val.courseName}
                        coverImage={val.coverImage}
                        duration={val.duration}
                        level={val.level}
                        discountPrice={val.discountPrice}
                        price={val.price}
                        teacherName={val.teacherName}
                        teacherPhoto={val.teacherPhoto}
                    />
                })}
            </Grid>
        </Container>
    );
}

export default RecommendedCourses