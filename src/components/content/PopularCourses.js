import { Container, Grid, Typography } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import CourseCard from "../card/CourseCard";

const PopularCourses = () => {
    const [courses, setCourses] = useState([])

    const axiosCall = async (url, body) => {
        const response = await axios(url, body);
        return response.data;
    };

    const getAllHandle = () => {
        axiosCall("https://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses")
            .then(result => saveDataToCoursePopular(result))
            .catch(error => console.log('error', error.message));
    };

    const saveDataToCoursePopular = (data) => {
        const popularData = data.filter(val => {
            return val.isPopular;
        })
        setCourses(popularData);
    };

    useEffect(() => {
        getAllHandle()
    }, []);


    return (
        <Container maxWidth="lg" >
            {/* {getAllHandle()} */}
            <Typography variant="h5" mt={5} pl={3}>Popular</Typography>
            <Grid container>
                {courses.slice(0, 4).map((val, i) => {
                    return <CourseCard key={i}
                        courseName={val.courseName}
                        coverImage={val.coverImage}
                        duration={val.duration}
                        level={val.level}
                        discountPrice={val.discountPrice}
                        price={val.price}
                        teacherName={val.teacherName}
                        teacherPhoto={val.teacherPhoto}
                    />
                })}
            </Grid>
        </Container>
    );
};

export default PopularCourses;