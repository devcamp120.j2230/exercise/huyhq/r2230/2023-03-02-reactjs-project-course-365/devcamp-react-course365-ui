import { Container } from "@mui/material";
import ContentSlideComponent from "./ContentSlideComponent";
import PopularCourses from "./PopularCourses";
import RecommendedCourses from "./RecommendedCourses.js";

const ContentComponent = ()=>{
    return(
        <Container maxWidth={false} disableGutters>
            <ContentSlideComponent/>
            <RecommendedCourses/>
            <PopularCourses/>
        </Container>
    )
};
export default ContentComponent