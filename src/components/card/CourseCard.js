import { faBookmark, faClock } from "@fortawesome/free-regular-svg-icons";
import { } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Grid, Card, CardContent, Typography } from "@mui/material";

const CourseCard = (props) => {
    const courseName = props.courseName;
    const coverImage = props.coverImage;
    const duration = props.duration;
    const level = props.level;
    const discountPrice = props.discountPrice;
    const price = props.price;
    const teacherName = props.teacherName;
    const teacherPhoto = props.teacherPhoto;

    return (
        <Grid xs={12} lg={3} md={6} sm={12} alignSelf="center" p={3}>
            <Card>
                <CardContent
                    component="img"
                    sx={{
                        maxWidth: "100%",
                        padding: 0
                    }}
                    src={require("../../asset/" + coverImage)} />
                <CardContent>
                    <Typography gutterBottom variant="h6" component="div" color="blue">
                        {courseName}
                    </Typography>
                    <Typography component="div">
                        <Typography variant="span" mr={1}>
                            <FontAwesomeIcon icon={faClock} />
                        </Typography>
                        <Typography variant="span" color="text.secondary" mr={1}>
                            {duration}
                        </Typography>
                        <Typography variant="span" color="text.dark">
                            {level}
                        </Typography>
                    </Typography>
                    <Typography component="div" mt={3}>
                        <Typography variant="span" color="text.dark" mr={1} sx={{ fontWeight: 700 }}>
                            ${discountPrice}
                        </Typography>
                        <Typography variant="span" color="text.secondary" sx={{ textDecoration: "line-through" }}>
                            ${price}
                        </Typography>
                    </Typography>
                </CardContent>
                <CardContent sx={{ borderTop: "1px solid #dee2e6" }}>
                    <Grid container alignSelf="center">
                        <Grid xs={2}>
                            <Typography
                                component="img"
                                item
                                src={require("../../asset/" + teacherPhoto)}
                                sx={{

                                    width: "100%",
                                    borderRadius: "50%",
                                }}
                            />
                        </Grid>
                        <Grid xs={8} variant="span" color="text.dark" alignSelf="center" pl={1}>
                            {teacherName}
                        </Grid>
                        <Grid xs={2} variant="span" alignSelf="center">
                            <FontAwesomeIcon icon={faBookmark} />
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </Grid>
    );
}

export default CourseCard;