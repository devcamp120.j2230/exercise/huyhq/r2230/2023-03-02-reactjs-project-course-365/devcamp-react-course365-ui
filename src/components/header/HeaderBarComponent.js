import { AppBar, Container, Toolbar, Typography } from "@mui/material";
import logo from "../../asset/images/Ionic_logo.png"
const HeaderBarComponent = () => {
    return (
        <AppBar position="static" color="transparent" enableColorOnDark>
            <Container maxWidth={false}>
                <Toolbar>
                    <Typography
                        href="#"
                        component="img"
                        src={logo}
                        sx={{
                            mr: 2,
                            display: { md: 'flex' },

                        }} />
                    <Typography
                        href="#"
                        component="a"
                        sx={{
                            mr: 2,
                            display: { md: 'flex' },
                            fontSize: '20px',
                            color: 'black',
                            textDecoration: 'none',
                        }}>
                        Home
                    </Typography>
                    <Typography
                        href="#"
                        component="a"
                        sx={{
                            mr: 2,
                            display: { md: 'flex' },
                            fontSize: '20px',
                            color: 'grey',
                            textDecoration: 'none',
                        }}>
                        Browse courses
                    </Typography>
                    <Typography
                        href="#"
                        component="a"
                        sx={{
                            mr: 2,
                            display: { md: 'flex' },
                            fontSize: '20px',
                            color: 'grey',
                            textDecoration: 'none',
                        }}>
                        About Us
                    </Typography>
                </Toolbar>
            </Container>
        </AppBar>

    );
};

export default HeaderBarComponent;