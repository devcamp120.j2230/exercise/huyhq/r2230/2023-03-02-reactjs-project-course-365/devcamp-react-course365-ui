import { Container, Grid, List, ListItem, ListItemButton, ListItemText, Typography } from "@mui/material";

const FooterComponent = () => {
    return (
        <Container maxWidth="lg">
            <Grid container spacing={1} mt={5}>
                <Grid xs={8} textAlign="center" alignSelf="center">
                    <Typography
                        component="span"
                        color="#6c757d"
                    >© 2021 Ionic Course365. All Rights Reserved.</Typography>
                </Grid>
                <Grid xs={4} textAlign="center">
                    <List>
                        <ListItem textAlign="center">
                            <ListItemButton component="a" href="#">
                                <ListItemText primary="Privacy"/>
                            </ListItemButton>
                            <ListItemButton component="a" href="#">
                                <ListItemText primary="Terms"/>
                            </ListItemButton>
                            <ListItemButton component="a" href="#">
                                <ListItemText primary="Feedback"/>
                            </ListItemButton>
                            <ListItemButton component="a" href="#">
                                <ListItemText primary="Support"/>
                            </ListItemButton>
                        </ListItem>
                    </List>
                </Grid>
            </Grid>
        </Container>
    );
}

export default FooterComponent;