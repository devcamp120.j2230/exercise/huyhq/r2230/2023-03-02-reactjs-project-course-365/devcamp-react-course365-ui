import { Button, Grid, Typography } from "@mui/material";
import img from "../../asset/images/ionic-207965.png";

const ContentSlideComponent = () => {
    return (
        <Grid container bgcolor="lightBlue">
            <Grid xs={6} alignSelf="center" px={10}>
                <Typography variant="h3" my={3} color="white">Welcome to Ionic Course365 Learning Center</Typography>
                <Typography variant="p" my={3} color="#6c757d">Ionic Course365 is an online learning and teaching marketplace with over 5000 courses and 1 million students. Instructor and expertly crafted courses, designed for the modern students and entrepreneur.</Typography>
                <Grid container my={3} spacing={3}>
                    <Grid>
                        <Button variant="contained" style={{background: "orange"}}>Browse Courses</Button>
                    </Grid>
                    <Grid>
                    <Button variant="contained">Become an Instructor</Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid xs={6} textAlign="center">
                <img src={img} />
            </Grid>
        </Grid>
    )
}
export default ContentSlideComponent;